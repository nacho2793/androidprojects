package com.example.ciac.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddActivity extends AppCompatActivity {

    EditText eID, eName, eLastName, eAge;
    String mId, mName, mLastName, mAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        eID=(EditText)findViewById(R.id.newID);
        eName=(EditText)findViewById(R.id.newName);
        eLastName=(EditText)findViewById(R.id.newLName);
        eAge=(EditText)findViewById(R.id.newAge);
    }

    public void saveData(View view){
        mId= eID.getText().toString();
        mName= eName.getText().toString();
        mLastName= eLastName.getText().toString();
        mAge= eAge.getText().toString();

        SaveOperation save = new SaveOperation(this);
        save.execute("add_info", mId, mName, mLastName, mAge);
        finish();
    }
}
