package com.example.ciac.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Debug;
import android.util.Log;

/**
 * Created by CIAC on 27/02/2017.
 */

public class DBManager extends SQLiteOpenHelper{

    private static final int DB_V = 1;
    private static final String DB_NAME = "claseSGM.db";
    private static final String CREATE_QUERY = "create table "+ClaseSGM.newStudent.TABLE_NAME
            +"("+ClaseSGM.newStudent.ID+" text,"+
            ClaseSGM.newStudent.NAME+" text,"+
            ClaseSGM.newStudent.LAST_NAME+" text,"+
            ClaseSGM.newStudent.AGE+" text);";

    DBManager(Context context){
        super(context, DB_NAME, null, DB_V);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
        Log.d("Database operation", "Database created...");

    }

    public void addInfo(SQLiteDatabase db, String id, String name, String last_name, String age){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ClaseSGM.newStudent.ID, id);
        contentValues.put(ClaseSGM.newStudent.NAME, name);
        contentValues.put(ClaseSGM.newStudent.LAST_NAME, last_name);
        contentValues.put(ClaseSGM.newStudent.AGE, age);
        db.insert(ClaseSGM.newStudent.TABLE_NAME, null, contentValues);
    }
    public Cursor getInfo(SQLiteDatabase db){
        String[] projections = {ClaseSGM.newStudent.ID,
                                ClaseSGM.newStudent.NAME,
                                ClaseSGM.newStudent.LAST_NAME,
                                ClaseSGM.newStudent.AGE};
        Cursor cursor = db.query(ClaseSGM.newStudent.TABLE_NAME, projections, null, null, null, null, null);
        return cursor;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
