package com.example.ciac.sqlite;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nacho on 2/27/2017.
 */

public class StudentAdapter extends ArrayAdapter{
    List mList = new ArrayList();
    public StudentAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(Student object) {
        mList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        StudentHolder studentHolder;
        if(row==null){
            LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.activity_display, parent, false);
            studentHolder = new StudentHolder();
            studentHolder.tv_id = (TextView)row.findViewById(R.id.tv_id);
            studentHolder.tv_name = (TextView)row.findViewById(R.id.tv_name);
            studentHolder.tv_last_name = (TextView)row.findViewById(R.id.tv_lastname);
            studentHolder.tv_age = (TextView)row.findViewById(R.id.tv_age);
            row.setTag(studentHolder);
        }else{
            studentHolder = (StudentHolder)row.getTag();
        }
        Student student = (Student) getItem(position);
        studentHolder.tv_id.setText(student.getId().toString());
        studentHolder.tv_name.setText(student.getName().toString());
        studentHolder.tv_last_name.setText(student.getLast_name().toString());
        studentHolder.tv_age.setText(student.getAge().toString());

        return row;
    }

    static class StudentHolder{
        TextView tv_id, tv_name, tv_last_name, tv_age;
    }
}
