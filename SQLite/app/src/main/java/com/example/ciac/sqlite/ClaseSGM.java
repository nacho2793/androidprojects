package com.example.ciac.sqlite;

/**
 * Created by CIAC on 27/02/2017.
 */

public final class ClaseSGM {

    ClaseSGM(){}

    public static abstract class newStudent{
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String LAST_NAME = "last_name";
        public static final String AGE = "age";
        public static final String TABLE_NAME = "Student_table";
    }
}
