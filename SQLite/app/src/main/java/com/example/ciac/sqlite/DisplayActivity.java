package com.example.ciac.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DisplayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        SaveOperation getDisplay = new SaveOperation(this);
        getDisplay.execute("get_info");
    }

}
