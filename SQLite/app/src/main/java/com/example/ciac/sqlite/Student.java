package com.example.ciac.sqlite;

/**
 * Created by Nacho on 2/27/2017.
 */

public class Student {
    private String id, name, last_name, age;

    public Student(String id, String name, String last_name, String age) {
        this.id = id;
        this.name = name;
        this.last_name = last_name;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
