package com.example.ciac.sqlite;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by CIAC on 27/02/2017.
 */

public class SaveOperation extends AsyncTask<String, Student, String> {
    Context context;
    StudentAdapter studentAdapter;
    Activity activity;
    ListView listView;
    SaveOperation(Context mContext){
        context=mContext;
        activity=(Activity) mContext;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result.equals("get_info")){
            listView.setAdapter(studentAdapter);
        }else{
            Toast.makeText(context, "Alumno Guardado", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onProgressUpdate(Student... values) {
        super.onProgressUpdate(values);
        studentAdapter.add(values[0]);

    }

    @Override
    protected String doInBackground(String... params) {
        String method = params[0];
        DBManager dbManager = new DBManager(context);
        if(method.equals("add_info")){
            String Id = params[1];
            String Name = params[2];
            String LastName = params[3];
            String Age = params[4];
            SQLiteDatabase db = dbManager.getWritableDatabase();
            dbManager.addInfo(db,Id, Name, LastName, Age);
        }
        else if (method.equals("get_info")){
            listView = (ListView) activity.findViewById(R.id.activity_display);
            SQLiteDatabase db = dbManager.getReadableDatabase();
            Cursor cursor = dbManager.getInfo(db);
            studentAdapter = new StudentAdapter(context, R.layout.display_student_row);
            String id, name, last_name, age;
            while(cursor.moveToNext()){
                id = cursor.getString(cursor.getColumnIndex(ClaseSGM.newStudent.ID ));
                name = cursor.getString(cursor.getColumnIndex(ClaseSGM.newStudent.NAME ));
                last_name = cursor.getString(cursor.getColumnIndex(ClaseSGM.newStudent.LAST_NAME ));
                age = cursor.getString(cursor.getColumnIndex(ClaseSGM.newStudent.AGE ));
                Student student = new Student(id, name, last_name, age);
                publishProgress(student);
            }
            return "get_info";
        }
        return null;
    }
}
