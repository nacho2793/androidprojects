package com.example.nacho.datapersistance_sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void goLocal(View view){
        startActivity(new Intent(this, LocalStorageActivity.class));
    }
    public  void goDB(View view){
        startActivity(new Intent(this, DBActivity.class));
    }
}
