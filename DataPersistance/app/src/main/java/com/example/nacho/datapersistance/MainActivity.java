package com.example.nacho.datapersistance;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private static final String DATA_FILE_NAME = "Storage01.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void saveText(View view){
        EditText editText = (EditText)findViewById(R.id.editText);
        String text = editText.getText().toString();
        editText.setText("");
        writeTextInFile(DATA_FILE_NAME, text);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void readText(View v){
        String message = "Read the string " + readTextInFile(DATA_FILE_NAME);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void writeTextInFile(String DATA_FILE_NAME, String message){
        try (FileOutputStream os = openFileOutput(DATA_FILE_NAME, Context.MODE_PRIVATE)) {
            os.write(message.getBytes());
            System.out.println("Wrote the string " + message + " to file " +
                    DATA_FILE_NAME);
        } catch (IOException e) {
            System.out.println("Failed to write " + DATA_FILE_NAME + " due to " + e);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String readTextInFile(String DATA_FILE_NAME){
        File where = getFilesDir();
        System.out.println("Our private dir is " + where.getAbsolutePath());

        try (BufferedReader is = new BufferedReader(
                new InputStreamReader(openFileInput(DATA_FILE_NAME)))) {
            String line = is.readLine();
            return line;
        } catch (IOException e) {
            System.out.println("Failed to read back " + DATA_FILE_NAME + " due to " + e);
        }
        return null;
    }


}
