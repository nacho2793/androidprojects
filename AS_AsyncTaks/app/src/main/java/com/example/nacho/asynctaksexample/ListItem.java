package com.example.nacho.asynctaksexample;

/**
 * Created by hp user on 24/02/2017.
 */

public class ListItem {
    int id;
    String title;

    public ListItem(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public String toString() {
        return title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}

