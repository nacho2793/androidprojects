package com.example.nacho.asynctaksexample;

import android.app.LauncherActivity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button mButton;
    ProgressBar mProgressBar;
    ListView mListView;
    ArrayList<ListItem> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.listviewid);
        mButton = (Button)findViewById(R.id.button);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar2);

        mButton.setOnClickListener(this);
    }

    public void callWS(View w) {
        new readjson().execute("0163473.upweb.site", "3000", "/asynctask");
    }

    public static String converse(String host, int port, String path) throws IOException {
        URL url = new URL("http", host, port, path);
        URLConnection conn = url.openConnection();
        // This does a GET; to do a POST, add conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setAllowUserInteraction(true); // useless but harmless
        conn.connect();

        // To do a POST, you'd write to conn.getOutputStream());

        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }




    private void UnSegundo(){
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                readjson takeNames = new readjson();
                takeNames.execute("0163473.upweb.site", "3000", "/alumnos");
        }
    }

    private class readjson extends AsyncTask<String, Integer, JSONObject>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setMax(100);
            mProgressBar.setProgress(0);
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(converse(params[0], Integer.parseInt(params[1]), params[2]));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jObject;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                JSONArray jObject = result.getJSONArray("urls");
                for (int i=0; i < jObject.length(); i++)
                {
                    try {
                        String url = jObject.getString(i);
                        results.add(new ListItem(i, url));
                        System.out.println(url);
                    } catch (JSONException e) {
                        // Oops
                        System.out.println("Failed to parse object");
                    }
                }
                mListView.setAdapter(new ArrayAdapter<ListItem>(MainActivity.this, R.layout.list_item, results));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



    }


    /*public class AsyncTaskClass extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setMax(100);
            mProgressBar.setProgress(0);
        }
        @Override
        protected Void doInBackground(Void... params) {
            for(int i=1;i<11;i++){
                UnSegundo();
                publishProgress(i*10);
            }
            return null;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mProgressBar.setProgress(values[0].intValue());
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getBaseContext(), "Task finished",Toast.LENGTH_LONG).show();
        }
    }*/
    /*
[{
Clase: "Soluciones Graficas para Moviles",
Profesor: "Luis Arias",
Semestre: "Sexto",
Carrera: "IAD",
Alumno: "Ignacio Sainz"
}

,{
Clase: "Soluciones Graficas para Moviles",
Profesor: "Luis Arias",
Semestre: "Sexto",
Carrera: "IAD",
Alumno: "Ignacio Sainz"
}
,{
Clase: "Soluciones Graficas para Moviles",
Profesor: "Luis Arias",
Semestre: "Sexto",
Carrera: "IAD",
Alumno: "Ignacio Sainz"
}
,{
Clase: "Soluciones Graficas para Moviles",
Profesor: "Luis Arias",
Semestre: "Sexto",
Carrera: "IAD",
Alumno: "Ignacio Sainz"
}
c
]
 jObject = new JSONObject(converse(params[0], Integer.parseInt(params[1]), params[2]));

for(i=0;i<jObject.length;i++){
	 mObject = 	jObject.getObject(i);
	 String mClase = mObject.getString("Clase");

}



JSONObject jObject = null;*/

}
