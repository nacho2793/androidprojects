package com.example.ciac.foodadministrator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.facebook.login.LoginManager;

public class SetFoodActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_food);
        if(AccessToken.getCurrentAccessToken() == null){
            goLoginScreen();
        }
        Profile profile = Profile.getCurrentProfile();
        String name = profile.getName();
        TextView displayName = (TextView)findViewById(R.id.IDmyName);
        displayName.setText("Comidas de " + name);
    }

    private void goLoginScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.recetas){
            startActivity(new Intent(this, MyRecepiesActivity.class));
        }else if(id==R.id.compras){
            startActivity(new Intent(this, ShoppingActivity.class));
        }else if(id==R.id.IDcerrarSesion){
            LoginManager.getInstance().logOut();
            goLoginScreen();
        }
        //return super.onOptionsItemSelected(item);
        return true;
    }
}
