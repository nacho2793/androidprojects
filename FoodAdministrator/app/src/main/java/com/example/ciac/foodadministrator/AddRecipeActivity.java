package com.example.ciac.foodadministrator;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Array;

import static android.R.attr.onClick;
import static android.R.attr.start;
import static android.icu.text.DisplayContext.LENGTH_SHORT;

public class AddRecipeActivity extends AppCompatActivity {
    String nameToSave;
    JSONArray ingredientes;
    EditText ingrediente, cantidad;
    String[] newRecepy;
    String[] pasos;
    EditText paso;
    int numeroDeReceta = 1;

    EditText recepyName;
    TextView type;

    Button AddIngredient, AddStep, AddRecepy;
    int NumeroDeRecetas = 3;
    Button tipoReceta;
    int numTipo=0;
    String[] tiposDeRecetas = {
        "Desayuno", "Comida", "Cena", "Postre", "Otro"
    };
    String fileName;
    Receta NuevaReceta;


    String sessionID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);
        Profile profile = Profile.getCurrentProfile();
        sessionID = profile.getName();

        type=(TextView)findViewById(R.id.displayType);

        ingrediente = (EditText)findViewById(R.id.IDingrediente);
        cantidad = (EditText)findViewById(R.id.IDcantidad);
        paso = (EditText)findViewById(R.id.IDpaso);
        recepyName = (EditText)findViewById(R.id.IDrecepyname);

        AddIngredient = (Button)findViewById(R.id.bIngredient);
        AddStep = (Button)findViewById(R.id.bStep);
        AddRecepy = (Button)findViewById(R.id.bRecepy);
        tipoReceta = (Button)findViewById(R.id.IDtype);



        AddIngredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ingrediente.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Escriba el ingrediente", Toast.LENGTH_SHORT).show();
                }else{
                    JSONObject newIngredient = new JSONObject();
                    try {
                        newIngredient.put(ingrediente.getText().toString(), cantidad.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ingredientes.put(newIngredient);
                    Toast.makeText(getApplicationContext(),"Se agregó "+ingrediente.getText().toString()+" a la receta",Toast.LENGTH_SHORT ).show();
                    ingrediente.setText("");
                    cantidad.setText("");
                }

            }
        });
        AddStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numeroPaso=1;
                if(paso.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Campo Incompleto", Toast.LENGTH_SHORT).show();
                }else{
                    JSONObject newStep = new JSONObject();
                    try {
                        newStep.put("Paso "+numeroPaso, paso.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   //pasos.put(newStep);
                    Toast.makeText(getApplicationContext(),"Paso "+numeroPaso + " guardado",Toast.LENGTH_SHORT ).show();
                    numeroPaso+=1;
                    paso.setText("");
                    paso.setHint("Paso "+numeroPaso);
                }

            }
        });
        AddRecepy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = recepyName.getText().toString();
                String tipo = type.getText().toString();
                //NuevaReceta = new Receta(nombre, tipo, nombe, nobre);
                new saveDatainDB().execute(nombre);
            }
        });
        tipoReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(numTipo!=4){
                    numTipo+=1;
                }else {
                    numTipo=0;
                }

                type.setText(tiposDeRecetas[numTipo]);
            }
        });
    }
    public class saveDatainDB extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Receta Guardada Exitosamente", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(AddRecipeActivity.this, MyRecepiesActivity.class));
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected Void doInBackground(String... params) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference main = database.getReference();

            numeroDeReceta+=1;
            //main.child("Recetas").child(sessionID).child(params[0]).child("Tipo").setValue("Desayuno");
            main.child("Recetas").child(sessionID).push().setValue(params[0]);
            return null;
        }
    }
    @IgnoreExtraProperties
    public class Receta {

        public String Rname;
        public String Rtype;
        public String[] Ringredients;
        public String[] Rsteps;

        public Receta() {
            // Default constructor required for calls to DataSnapshot.getValue(User.class)
        }

        public Receta(String newName, String newType, String[] newIng, String[] newStep) {
            this.Rname = newName;
            this.Rtype = newType;
            for(int i=0;i<newIng.length;i++){
                Ringredients[i]="Paso " + (i+1) + ":" + newIng[i];
            }
            for(int i=0;i<newStep.length;i++){
                newStep[i]="Paso " + (i+1) + ":" + newStep[i];
            }
        }

    }
}

