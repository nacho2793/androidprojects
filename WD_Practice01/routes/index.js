var express = require('express');
var router = express.Router();

/* GET home page. */
var Nacho = {
  "Nombre":"Ignacio",
  "Edad":23,
  "Carrera": "IME / IAD",
  "Sexo": "Masculino"
}
var Dugy = {
  "Nombre":"Douglas",
  "Edad":21,
  "Carrera": "IAD",
  "Sexo": "Masculino"
}
var HoreJe = {
  "Nombre":"Jorge",
  "Edad":22,
  "Carrera": "IAD",
  "Sexo": "Masculino"
}
var Jos = {
  "Nombre":"Jocelyn",
  "Edad":21,
  "Carrera": "IAD",
  "Sexo": "Femenino"
}
var Ana = {
  "Nombre":"Ana Isabel",
  "Edad":20,
  "Carrera": "IAD",
  "Sexo": "Femenino"
}

var AppWebs;

AppWebs = [Nacho, Dugy, HoreJe, Ana, Jos];

router.get('/integrantes', function(req, res, next) {
    res.json(AppWebs);
    //res.render('integrantes',{alumnos: AppWebs});
});
router.get('/integrantes/Nacho', function(req, res, next) {
    res.json(AppWebs[4].Nombre);
    //res.render('integrantes',{alumnos: AppWebs});
});
module.exports = router;
