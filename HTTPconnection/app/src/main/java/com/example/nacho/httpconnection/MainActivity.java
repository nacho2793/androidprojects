package com.example.nacho.httpconnection;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private TextView mText;
    private ProgressBar mBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button mButton = (Button)findViewById(R.id.button);
        mText = (TextView)findViewById(R.id.jsonParsed);
        mBar = (ProgressBar)findViewById(R.id.progressBar);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new getJSON().execute("http://0163473.upweb.site:3030/alumnos");
            }
        });



    }

    public class getJSON extends AsyncTask<String, Integer, String>{
        @Override
        protected void onPreExecute() {
            mBar.setMax(100);
            mBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream stream;
            BufferedReader reader = null;
            StringBuffer buffer;
            String finalText="";
            HttpURLConnection connection = null;
            try{
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                buffer = new StringBuffer();

                String line="";
                while((line = reader.readLine()) != null){
                    buffer.append(line);
                }

                String finalJSON = buffer.toString();
                JSONObject classes = new JSONObject(finalJSON);
                JSONObject profesor = classes.getJSONObject("Profesor");
                String nombre = profesor.getString("Nombre");
                JSONArray alumnos = classes.getJSONArray("Alumnos");
                finalText+="Profesor: " + nombre + "\n------------------------------\n";
                for(int i=0;i<alumnos.length();i++){
                    JSONObject alumno = alumnos.getJSONObject(i);
                    nombre = alumno.getString("Nombre");
                    finalText += "Alumno " + (i+1) + ": " + nombre + "\n";
                    publishProgress((i+1)*20);
                    UnSegundo();
                }
                return finalText;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if(connection!=null) {
                    connection.disconnect();
                }
                try {
                    if(reader!=null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mBar.setProgress(values[0].intValue());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mBar.setProgress(0);
            mText.setText(result);
        }
    }
    private void UnSegundo(){
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){}
    }
}
