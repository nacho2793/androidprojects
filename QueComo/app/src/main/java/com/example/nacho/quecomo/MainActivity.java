package com.example.nacho.quecomo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    Spinner place;
    Spinner food;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        place = (Spinner)findViewById(R.id.spinnerPlace);
        food = (Spinner)findViewById(R.id.spinnerFood);
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.Places));
        mAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        place.setAdapter(mAdapter);
        mAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.Foods));
        mAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        food.setAdapter(mAdapter);
    }
}
